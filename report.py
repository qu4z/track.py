#!/usr/bin/python3
import sqlite3
import argparse
import datetime

# TODO
#  - Add aggregation without pids if necessary (or just by command name/args[0])

parser = argparse.ArgumentParser(description='Report on track.py\'s data')
parser.add_argument("--date", "-d", help="The day for which to print info, eg 2019-01-08")
parser.add_argument("--by-time", "-t", help="Sort by time spent on each entry", action="store_true")
parser.add_argument("--by-name", "-n", help="Sort alphabetically", action="store_true")
parser.add_argument("process_number", help="Filter by an ID if provided. Obtained from the first column.", nargs="?")
args = parser.parse_args()

def get_process_data(conn, date, sort):

    sort_key = "proc.args ASC" # process name + args
    if sort is "time":
        sort_key = "COUNT(p.date) DESC"
            
    c = conn.cursor()
    c.execute('''SELECT proc.id, proc.pid, proc.args, COUNT(p.date)
            FROM points AS p 
            INNER JOIN processes AS proc 
            ON p.process = proc.id 
            WHERE date(p.date) == ?
            GROUP BY proc.id, date(p.date)
            ORDER BY ''' + sort_key, [date,])
    processes = c.fetchall()
    return processes

def display_process_data(date, p):
    print(date)
    print('-' * len(str(date)))
    print()

    print("ID\tTIME\tPID\tCOMMAND\t")
    for row in p:
        time = datetime.timedelta(seconds=row[3])
        print(str(row[0]) + "\t" + str(time) + "\t" + str(row[1]) + "\t" + str(row[2]))
    if not p:
        print("<No entries>")
    print()

def get_activity_data(conn, date, proc, sort):

    sort_key = "a.title ASC" # process name + args
    if sort is "time":
        sort_key = "COUNT(p.date) DESC"
            
    c = conn.cursor()
    c.execute('''SELECT a.title, COUNT(p.date)
            FROM points AS p 
            INNER JOIN activities AS a
            ON p.activity == a.id
            WHERE date(p.date) == ? AND p.process == ?
            GROUP BY date(p.date), a.title
            ORDER BY ''' + sort_key, [date,proc])
    processes = c.fetchall()
    return processes

def display_activity_data(date, a):
    print(date)
    print('-' * len(str(date)))
    print()

    print("TIME\tACTIVITY")
    for row in a:
        time = datetime.timedelta(seconds=row[1])
        print(str(time) + "\t" + row[0])
    if not a:
        print("<No entries>")
    print()


conn = sqlite3.connect('timetracking.db')

date = datetime.datetime.now().date()
sort = "time"
activities_process = args.process_number

if args.date:
    date = datetime.datetime.strptime(args.date, '%Y-%m-%d').date()
if args.by_name:
    sort = "name"
if args.by_time:
    sort = "time"

if not activities_process:
    processes = get_process_data(conn, date, sort)
    display_process_data(date, processes)
else:
    activities = get_activity_data(conn, date, activities_process, sort)
    display_activity_data(date, activities)

conn.close()
