#!/usr/bin/python3

import signal
import sqlite3

from subprocess import run
from time import sleep
from datetime import datetime

# TODO
#  - Improve bash calls to fix injection issues


# Gathering data

def shellout(*args):
    result = run(args, capture_output=True)
    return result.stdout.decode("utf-8").strip()

def gather_data():
    r = {}
    r["win"] = shellout("xdotool", "getwindowfocus")
    r["name"] = shellout("xdotool", "getwindowname", r["win"])
    r["pid"] = shellout("xdotool", "getwindowpid", r["win"])
    r["args"] = shellout("bash", "-c", "ps -u -p " + r["pid"] + " | cut -c 66- | tail -n +2")
    # TODO: This needn't be bash.
    r["date"] = datetime.now()
    return r

def process_data(i):
    o = {}
    o["pid"] = i["pid"]
    o["args"] = i["args"]
    o["date"] = i["date"]

    o["activity"] = None
    if ("firefox" in i["args"]):
        o["activity"] = i["name"]
    if ("urxvt" in i["args"]):
        bash_pid = shellout("pgrep", "-P", i["pid"])
        o["activity"] = shellout("readlink", "-e", "/proc/" + bash_pid + "/cwd")
    return o

# Database

def create_database_if_not_exists(conn):
    conn.execute('''CREATE TABLE IF NOT EXISTS processes
            (id INTEGER PRIMARY KEY, pid TEXT NOT NULL, args TEXT NOT NULL,
            CONSTRAINT "unique" UNIQUE(pid, args))''')
    conn.commit()
    
    conn.execute('''CREATE TABLE IF NOT EXISTS activities
            (id INTEGER PRIMARY KEY, process INTEGER NOT NULL, title TEXT NOT NULL,
            CONSTRAINT "unique" UNIQUE(process, title))''')
    conn.commit()
    
    conn.execute('''CREATE TABLE IF NOT EXISTS points
            (id INTEGER PRIMARY KEY, 
             date DATETIME NOT NULL, 
             process INTEGER NOT NULL, 
             activity INTEGER)''')
    conn.commit()

def write_data(conn, data):
    c = conn.cursor()
    c.execute("INSERT OR IGNORE INTO processes (pid, args) VALUES (?, ?)", [data["pid"], data["args"]])
    c.execute("SELECT id FROM processes WHERE pid = ? AND args = ?", [data["pid"], data["args"]])
    process = c.fetchone()[0]

    activity = None
    if data["activity"] is not None:
        c.execute("INSERT OR IGNORE INTO activities (process, title) VALUES (?, ?)", [process, data["activity"]])
        c.execute("SELECT id FROM activities WHERE process = ? AND title = ?", [process, data["activity"]])
        activity = c.fetchone()[0]

    c.execute("INSERT INTO points (date, process, activity) VALUES (?, ?, ?)", [data["date"], process, activity])
    print(process, activity)

    conn.commit()


# Main code

running = True
def signal_int_handler(signal, frame):
    global running
    print("Exiting...")
    running = False
signal.signal(signal.SIGINT, signal_int_handler)



conn = sqlite3.connect('timetracking.db')
create_database_if_not_exists(conn)

while running:
    raw_data = gather_data()
    data = process_data(raw_data)
    write_data(conn, data)
    sleep(1)

conn.close()

